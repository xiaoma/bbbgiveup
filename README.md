# BeagleBone Black 从入坑到弃坑

#### 介绍
BeagleBone Black 开发笔记

BeagleBone Black（以下简称BBB） 是开源社区组织 BeagleBoard.org 推出的一款基于 TI 公司 AM3358 处理器的开源开发套件，处理器集成了高达 1GHz 的 ARM Cortex-A8 内核，并提供了丰富的外设接口。开发者可快速地将构想转化为产品。

你可以访问其[官方社区](https://beagleboard.org/)主页。这是深入了解这些平台复杂细节的最佳方式。这也使得你更好的评估究竟BBB适不适合你的项目。

本项目包含了一系列的开发笔记，以及开发过程中用到的代码。

(一) [初见乍惊欢](01.md)


